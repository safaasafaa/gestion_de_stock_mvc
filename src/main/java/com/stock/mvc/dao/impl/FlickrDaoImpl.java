package com.stock.mvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.stock.mvc.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao{
	
	private Flickr flickr;
	
	private UploadMetaData uploadMetaData = new UploadMetaData();
	
	private String apiKey = "022575f6cfbbe0bcfc5784450f7ce5cf";
	
	private String sharedSecret = "869af81f4aae27dd";
	
	private void connect() {
		flickr = new Flickr(apiKey,sharedSecret,new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157677216264438-be16830a7f275904");
		auth.setTokenSecret("d614fe51da7b5667");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}

	@Override
	public String savePhoto(InputStream stream, String fileName) throws Exception{
		connect();
		uploadMetaData.setTitle(fileName);
		String photoId = flickr.getUploader().upload(stream, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	
	public void auth() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		
		AuthInterface authInterface = flickr.getAuthInterface();
		
		Token token = authInterface.getRequestToken();
		System.out.println("token: " + token);
		
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to authorize yourself on Flickr");
		System.out.println(url);
		System.out.println("Paste in the token it gives you:");
		System.out.println(">>");
		
		String tokenKey = JOptionPane.showInputDialog(null);
		
		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println("Authentification Success");
		
		Auth auth=null;
		
		try {
			auth=authInterface.checkToken(requestToken);
		}catch(FlickrException e) {
			e.printStackTrace();
		}
		
		System.out.println("Token: "+requestToken.getToken());
		System.out.println("Secret: "+ requestToken.getSecret());
		System.out.println("nsid:"+auth.getUser().getId());
		System.out.println("Realname"+auth.getUser().getRealName());
		System.out.println("Username"+auth.getUser().getUsername());
		System.out.println("Permission"+auth.getPermission().getType());
	}

}
